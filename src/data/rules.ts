export const startingDesc = `<h3 class="font-bold text-2xl">
Basic Rules
</h3>
<h4 class="font-bold text-xl pt-4 pb-2">Intensity</h4>
<p>Your Intensity is the first step to creating a build. Intensity is a single number that reflects your desired tone, from a cozy retirement at Intensity 0 to a grueling struggle against insurmountable odds at Intensity 10. At lower intensities, your starting budget will be higher, but your ongoing gains from captures and sales will be much lower, to a minimum of zero at Intensity 0.</p>
<p>To find your Intensity value, go to <a class="text-green-800 dark:text-green-400 hover:underline" href="https://waifu-catalog.neocities.org/intensity/difficulty">Negotiations</a> and make your choices from among those options.</p>
`

export const startingWorld = `<h4 class="font-bold text-xl pt-4 pb-2">Your Starting World</h4>
<p>The second step to creating a build is determining your starting world. Every world has a Danger Rating (DR), measured on a scale from 1 to 10. These reflect our estimates of how dangerous the world is, taking into account the power of the typical combatants, the average danger level to ordinary people living in that world, and especially the actual cost of which of our perks might be the basic necessities to survive in that world. Entering a world during a major crisis or active apocalypse may raise a world’s rating by one or even two ranks, and some specific locations within a world may have their own DRs.</p>
<p>DR1 worlds are typically paradises, where even the local inhabitants are at little risk of trouble. DR2 is equivalent to the developed countries on an Information Age Earth. Danger and hardship still exist, but at the relatively low levels that most of our contractors are fаmiliar with. DR10 reflects the worst hellworlds, where life is brutal and short. Specific worlds and their ratings are listed <a href="/world" class="text-green-600 dark:text-green-500 underline">here</a>.</p>
<p>If your world is not on the list, compare it to the ones that are and use your best judgment. A world’s danger rating can be affected by AU elements, but any characters’ metaknowledge should not cover these changes.</p>`

export const startingBudget = `
<h4 class="font-bold text-xl pt-4 pb-2">Your Starting Budget</h4>
<p>Now that you know your Intensity and the DR of your starting world, you can match the two on the chart below to find your starting budget. This budget is only granted once. Traveling to a new world, region, or time period will not grant you additional credits, not even the difference between the lower DR and the higher. The following chart shows the budget for each DR at each of the major Intensity intervals.</p>
<p>Now that you know your budget, pick your Origin, one Binding-type control, and as many of the Lures, Other Controls, Perks, Companions, and Fаmiliars as you desire or can afford.</p>`

export const intensityTableTitles = ['Intensity', 'DR 1', 'DR 2', 'DR 3', 'DR 4', 'DR 5', 'DR 6', 'DR 7', 'DR 8', 'DR 9', 'DR 10']
export const intensityTable = [
  [0, 55, 90, 145, 230, 375, 610, 985, 1590, 2575, 4165],
  [1, 35, 55, 85, 140, 225, 365, 590, 955, 1545, 2500],
  [2, 25, 40, 60, 100, 160, 260, 420, 680, 1105, 1785],
  [3, 20, 30, 50, 75, 125, 205, 330, 530, 860, 1390],
  [4, 15, 25, 40, 65, 100, 165, 270, 435, 700, 1135],
  [5, 15, 20, 35, 55, 85, 140, 225, 365, 595, 950],
  [6, 10, 20, 30, 45, 75, 120, 195, 320, 515, 835],
  [7, 10, 15, 25, 40, 65, 105, 175, 280, 465, 735],
  [8, 10, 15, 25, 35, 60, 95, 155, 250, 405, 660],
  [9, 10, 15, 20, 35, 55, 85, 140, 225, 370, 595],
  [10, 5, 10, 20, 30, 50, 80, 130, 210, 335, 545],
]

export const yourDevice = `
<h3 class="font-bold text-xl">Your Contractor Smart Device</h3>
<p>No matter your choices, you will receive one and only one complimentary smart device of your choice, preloaded with the company’s shopping app so you can manage your purchases and trade-ins on the go. The exact nature of your device is fixed. You may, however, trade in your current device and request a replacement at any time. Your current device will disappear immediately, and the new one will arrive after seven days (168 hours).</p>
<p>The company shopping app’s listings include descriptions of each listed character, including all major ability and skill tags and a short summary of their personality, so you know exactly what you’re paying for. Rule 63, humanized, kemonomimi-ized, and other variants are available for every character at no extra cost, unless the new form’s abilities would be too different from their listed tier: the perk Power Swap covers those cases. You and your companions are the only ones who can perceive or use any company app, but only you can make any transactions through this one.</p>
<p>Your company smart device cannot be hacked. If it is ever broken, it will repair itself completely the next time it isn’t observed.  If it is ever more than 10m away from you or any of your companions for more than 15 minutes, it will teleport into your pocket. If the local space is too warped for that distance to be accurate, you may also recall it manually. The same principle applies in digital worlds, even if you have a physical body “outside” in meatspace. If one of your companions is using this device for any purpose and you ask for it back, she'll hand it over immediately without question.</p>
<p>Your smart device will automatically disintegrate upon your account’s closure. If you have zero subjects, this will happen immediately upon your death. Otherwise, we will wait until your R.I.P. timer runs out.</p>
`

export const glossary = `
<h3 class="font-bold underline text-lg text-center">Glossary</h3>
<div><b>Contractor</b>: You.</div>
<div><b>Retainers, Subjects</b>: Any living thing you own.</div>
<div><b>Companions</b>: Your waifus and/or husbandos, i.e. subjects that you intend to have intimate relations with.</div>
<div><b>Fаmiliars</b>: Any subject that is not a companion..</div>
<div><b>Retinue</b>: The group of you and all subjects.</div>
`

export const captures = `<h3 class="font-bold text-xl pt-4 pb-2">Captures</h3>
<p>Additional companions may be acquired by “capturing” them in their homeworlds. This is typically achieved by using a Binding or certain Other Controls, or when a target gives you a sincere love confession - usually romantic, but false positives occasionally happen and trigger a capture that isn’t entirely merited. This love may be assisted by any means, including the use of Lure-type controls or third-party mind control.</p>
<p>Confessions must either be delivered in-person or by a two-way live feed of any sort. The target does not need to be aware that you received their confession at the time it was given, but a target with full awareness of their surroundings should be reasonably expected to know that you could. The exact wording of any confession varies greatly with the target’s personality and cultural context.</p>
<p>When you capture a Tier 1-10 target, we’ll pay you a certain fraction of their purchase price of their effective tier at step 1a, rising with your Intensity and then rounded down to the next full credit. If, due to your retinue’s actions, their effective tier at step 3 is less than step 1a, we’ll use that value instead. Capture credits are not granted for local versions of targets that you’ve purchased or alternate versions that you’ve already captured once, unless they’re a unique entry in the lists proper or as a waifu perk. If a target is only listed as part of a group entry, you will only get the credits after capturing every member of the group.</p>
<p>If you capture a character “early,” before they’ve achieved the potential reflected in the version available for purchase, you will still receive their full capture value. As compensation for the subject’s relative weakness, we at the company will download the missing abilities and a framework for the missing skills into your capture target at no charge. This framework will also guide the capture to any missing equipment or fаmiliars, but you must hunt those down, and provide the subject with the training needed to master their new power, on your own time.</p>
`

export const captureExtra = `<h3 class="font-bold text-xl pt-4 pb-2">Capturing Eхtras</h3>
<p>Subjects not named on any list may also be captured for credits, provided they have at least three of the following in their original canon: name, personality or backstory, abilities or skills or powers, and appearance or design. As an author, you must be able to assert that the character definitely existed in canon, even as an extra in a crowd scene, and then rate their tiers yourself. Individuals who fail this test are worth 5% of the purchase price for their tier at step 1a. On low intensities, when the regular capture value is less than 5% of purchase price, they will be worth that instead.</p>
<p>Because captured extras lack a standard model for comparison, unlike listed entries, any additions that would normally be accounted for in steps 3 or 4 will instead be included in the newly-created standard model and calculated accordingly. If an extra is the first example of a generic entry that you’ve captured, they will still count for the full price of that entry, if it’s higher than the above.</p>
`

export const effectiveTiers = `<h3 class="font-bold">Effective Tiers</h3>
  <p>A person’s tier can be affected by outside sources - whether that means you, another Contractor, or some other quirk of fate. To calculate a person’s effective tier, follow these steps. A contractor’s life before meeting the company would be Step 0.</p>
  <ul>
    <li><b>1.</b>  Companion’s unmodified tier. This is the number given in their Catalog entry.
      <ul class="pl-4">
        <li><b>a.</b> Apply any waifu perks.</li>
        <li><b>b.</b> Apply any permanent bonuses from Heritages.</li>
      </ul>
    </li>
    <li><b>2.</b>  Account for cross-training (including the benefits from training with Talents), Added Potential, Template Stacking, and any sort of third-party augmentations (biological, cybernetic, spiritual, etc.). This step represents an individual’s state if they were suddenly freed from their contractor or otherwise not (or no longer) associated with the company… and also naked.</li>
    <li><b>3.</b>  Account for any special or empowering equipment that the individual has. This step represents the individual’s state if they were suddenly freed from their contractor or otherwise not (or no longer) associated with the company.</li>
    <li><b>4.</b>  Account for the direct effects of Binding-type controls.
      <ul class="pl-4">
        <li><b>a.</b> Account for the ongoing effects of all other persistent catalog options: Lures, Demiplane/Dungeon access, Talents and Defenses, and Other Perks. This is the individual’s usual state within a contractor’s retinue.</li> 
      </ul>
    </li>
    <li><b>5.</b>  Apply any conditional or other temporary effects from Heritage perks, personal abilities or other sources.</li>
  </ul>
  <p>Many individuals’ powers rely on henshin devices, fаmiliars, or special equipment that they can be easily separated from and reduced to, usually, only T1-T3. These items are accounted for in their listed tiers, but not included in their effective tier calculations until step 3.</p>`

export const familiars = `<h3 class="font-bold text-xl pt-4 pb-2">Fаmiliars</h3>
<p>Broadly speaking, a fаmiliar is any individual in your retinue that isn’t a member of your harem proper. This can include beloved pets, support staff, adopted children, people important to your harem that you aren’t interested in maintaining an emotional and sexual relationship with, or more traditional magical beasts such as Pokémon or magical girl mascots. Many purchasable companions are packaged with their fаmiliars and specified as such in the setting-specific rules; this is by no means a comprehensive list. Some fаmiliars named in the setting-specific rules are also present in the catalog listing as companions. If you capture a fаmiliar’s master first, you’ll get the fаmiliar too and receive the full value for both. Much like with humanoid captures, only the first copy of any type of generic magical or other beast is worth capture or sale credits.</p>
<p>Fаmiliars can be captured using local fаmiliar bonding methods, such as rituals or Pokéballs, or Binding or Other controls like anyone else, and are not subject to the romantic or sexual interest that your Bindings instill in your companions. If a humanoid fаmiliar is bound to you by local methods, they will count as captured for all purposes. No special methods are needed to distinguish a companion binding from a fаmiliar binding; intent is enough. Companion and Fаmiliar statuses are not permanent and may be changed in the app at any time. This change will always be intentional.</p>
`

export const device = `<h3 class="font-bold text-xl pt-4 pb-2">Your Contractor Smart Device</h3>
<p>No matter your choices, you will receive one and only one complimentary smart device of your choice, preloaded with the company’s shopping app so you can manage your purchases and trade-ins on the go. The exact nature of your device is fixed. You may, however, trade in your current device and request a replacement at any time. Your current device will disappear immediately, and the new one will arrive after seven days (168 hours).</p>
<p>Your company smart device cannot be hacked. If it is ever broken, it will repair itself completely the next time it isn’t observed.  If it is ever more than 10m away from you or any of your companions for more than 15 minutes, it will teleport into your pocket. If the local space is too warped for that distance to be accurate, you may also recall it manually. The same principle applies in digital worlds, even if you have a physical body “outside” in meatspace. If one of your companions is using this device for any purpose and you ask for it back, she'll hand it over immediately without question.</p>
<p>Your smart device will automatically disintegrate upon your account’s closure. If you have zero subjects, this will happen immediately upon your death. Otherwise, we will wait until your R.I.P. timer runs out.</p>
`

export const purchases = `<h3 class="font-bold text-xl pt-4 pb-2">Extending Your Build: Purchases</h3>
<p>The company’s Companions and Fаmiliars are all clones manufactured on demand, except where noted. They come packaged with their iconic outfits, personal equipment, and fаmiliars or treasured pets, when appropriate. The largest equipment normally allowed are motorcycles, but there are some rare exceptions. For your own ease of accounting, you may only purchase one copy of any distinct entry at a time.</p>
<p>Most companions are sorted into ten tiers, each of which has a different cost</p>
<p>All purchase prices are rounded up. Some Controls and Heritage perks include free copies of lesser perks, primarily Basic Talents and Defenses. These can ignore the normal prerequisites. If you’ve already purchased a perk before getting a free copy, you may take a discount on the new purchase equal to the free perk’s value. The total of all discounts applied to a single perk, unless it’s a Talent or Defense, is capped at -80% of the list price.</p>
<p>No powers found in this catalog will directly impact a Contractor’s own mind without an explicit warning otherwise (such as in Memoria). The company is not responsible for any power’s indirect effects on a Contractor’s decision-making.</p>
<p>For any member of your retinue whose powers come from an external, (semi-)sapient source, their connection will be severed at the moment of capture and those powers replicated as innate abilities.
If they have powers that rely on a background magical field, the connection to you will act as an adapter in worlds with a different field. In worlds or regions that lack any such field, their powers will still be usable, but at a reduced capacity when outside of your presence.</p>
<p>Many retinue members have powers that come with a crippling categorical weakness. This is more than just an unwanted limitation or inconvenience; green Kryptonite’s effects on a Kryptonian clearly qualifies, while Baldr’s vulnerability to mistletoe was from the plant being the only known thing that he lacked immunity to and thus does not. Altered dietary needs, such as many forms of vampiric thirst, are also not weaknesses in this sense. These individuals will be no more affected by qualifying weaknesses than an ordinary human.</p>
`

export const sales = `<h3 class="font-bold text-xl pt-4 pb-2">Extending Your Build: Sales</h3>
<p>If you only wanted a capture for their credit value, you may ship them to us at the company. We’ll pay you a certain fraction of the purchase price of their effective tier at step 3, rising with your Intensity and then rounded down to the next full credit. This applies regardless of whether step 1a or step 3 has the higher value. Humanoid fаmiliars gained for free from an Origin choice will not count for capture credits, but may still be sold at the normal rate. As with captures, payouts for sales are not granted for local versions of targets that you’ve purchased or alternate versions that you’ve already sold once, unless they’re a unique entry in the lists proper or as a waifu perk. Extras have no additional sale value.</p>
<p>Selling a subject only takes a single button press in the company app on your smart device. We’ll reprocess the captures and send them where they need to go. Otherwise, captures are yours to keep.</p>
`

export const waifu11 = `<h3 class="font-bold text-xl pt-4 pb-2">Imaginary Tiers</h3>
<p>Three more tiers exist beyond the normal ten: tier χ/X, Ψ/Y, and Ω/Z. These companions instead cost varying amounts of Imaginary Tickets (IMG), only available for starting builds on Danger Rating X. Both the Greek and Latin names for these Imaginary Tiers are acceptable. Imaginary Tier companions that are listed as Perks or Waifu Perks have further details in the appropriate sections. All Imaginary Tier companions are freely capable of multiversal travel, even if they showed no such abilities or explicitly could not in their original canons.</p>
<p>If, after your starting build, you ever find yourself with spare IMG and nothing or nobody you want to buy with it, you may exchange the tickets for 1000 credits each. This cannot be undone. </p>
<p>Captured Imaginary Tier companions may be shipped back to the company like any other. These follow similar capture and sale rates as regular companions, including the effects of Wage Slave, but are measured in both IMG and credits and convert every 1000c to 1 IMG. If you are not on Wage Slave, and your intensity is above 0, all sale values will also be worth 1 additional IMG.</p>
`

export const danger11 = `<h3 class="font-bold text-xl pt-4 pb-2">Danger Rating X Build Rules</h3>
<p>When creating a Danger Rating 11 build, ignore the basic build creation rules found above. You will instead start with the following:</p>
<ul class="list-disc list-inside">
  <li>One companion of Tier χ/X or Ψ/Y. Power Swapping a different companion to one of these tiers is allowed, as are Heavenbuilt Proto and Piece of Silver. You and your companion will have a Couple’s Account, following all of that option’s rules.</li>
  <li>Any Origin, free of charge. Substitute and Possess are limited to T6 and below.</li>
  <li>A single basic Binding-type control.</li>
  <li>As many official basic Lures as desired.</li>
  <li>All official Basic Talents and both purchases of all official Defenses</li>
  <li>A 400-credit budget for your Heritage.</li>
  <li>A 545-credit budget for Catch-a-Ride.</li>
  <li>A 500-credit budget for Demiplanes & Dungeons.</li>
  <li>A single 600-credit budget for all other official perks found in this catalog, including Binding and Lure expansions.</li>
</ul>
<p>Once your build is finalized, any leftover credits will carry forward to your regular account. This remainder may not be accessed before seven days (168 hours) have passed, but may be used for any purpose. Any Waifu Perks purchased for yourself in the initial build, including Power Swap, cannot raise your tier above 6; this only applies at that moment and not afterward.</p>
<p>You may start in any world you like, regardless of the above Danger restrictions. If you start in your companion’s homeworld, Yoink will automatically be applied at no cost. The prices of your captures and sales will be locked at their Intensity 10.0 values. Go anywhere you want, follow the special rules, and have fun.</p>
`

export const creditValue = `
<h3 class="font-bold text-xl pt-4 pb-2">PvP Credit Value</h3>
<p>This is the sum of the list prices for you and your retinue’s effective tiers after step 5, plus the combined list prices of your Controls, Catch-a-Ride vehicles, Talents & Defenses, and Demiplanes & Dungeons perks. Tier Xs are valued at their tickets' exchange rate of 2000 credits.</p>
`
export const pvpRules = `
  <h2 class="font-bold text-xl underline py-2">Other Contractors</h2>
  <p>The company is a multiversal organization. You are hardly their only contractor, though the multiverse is certainly expansive enough that you can live many lifetimes and never once meet one of your peers. The company offers a variety of means for non-allied contractors to interact, beyond random chance encounters in the vast multiverse. Some of these interactions will even be friendly, but others… less so.</p>

  <h3 class="font-bold text-xl pt-4 pb-2 mt-2">PvP Glossary</h3>
  <ul>
    <li class="list-inside list-disc">
    <b>Home Territory / Turf</b>: This includes all company provided spaces (Pocket Apartment and upgrades, Dragon Cabin, space claimed by Creep, etc., plus any locations in a local world that you or your retinue have possessed for at least 24 hours.
    </li>
    <li class="list-inside list-disc">
    <b>PvP Asset Value</b>: The sum of the list prices for your retinue’s effective tiers after step 4, plus the combined list prices of your Catch-a-Ride vehicles. Imaginary Tiers are valued according to the exchange rate of their IMG value. If you only bring a limited force into a PvP match, only they will count towards this.
    </li>
    <li class="list-inside list-disc">
    <b>PvP Credit Value</b>: Your total PvP Asset Value, plus the combined list prices of your Controls, Talents, Demiplanes & Dungeons perks, and the first level of every owned Defense.
    </li>
  </ul>

  <h3 class="font-bold text-xl pt-4 pb-2 mt-2">General PvP Rules</h3>
  <p>Not all of these rulesets apply to every form of PvP, but each applies to more than one, and will be referred back to as necessary. All debuffs in these rules are in place for any interactions between you (and your retinue) and your opponent (and their subjects). These debuffs do not apply to interactions between yourself and natives of your current universe.
  </p>

  <h3 class="font-bold text-xl pt-4 pb-2 mt-2">Home Territory Integration</h3>
  <i>Enabled for: Invasions (Defending Side), Gauntlet</i>
  <p>If you have a Pocket Apartment or higher, the location is integrated into the local environment for as long as you’re connected to that world. The locals will not notice its appearance or disappearance. Conjunction, Apportation, and Warranty Plan’s killswitch are all disabled while on your opponent’s home turf.</p>

  <h3 class="font-bold text-xl pt-4 pb-2 mt-2">Defense Perk Restrictions</h3>
  <i>Enabled for: Invasions (all), Arranged, Gauntlet</i>
  <p>The often-necessary Defense perks made available to Contractors for PvE activity are also strong enough to cause excessive frustration in PvP. To answer this concern, the immunity levels of all Defenses are disabled.</p>
  
  <h3 class="font-bold text-xl pt-4 pb-2 mt-2">D-Travel Restrictions</h3>
  <i>Enabled for: Invasions (all), Arranged, Gauntlet (certain conditions)</i>
  <p>To ensure that a match actually occurs, participants may not leave until the end, whether the intended destination is a parallel of the same world or part of a different multiverse cluster entirely.</p>
  
  <h3 class="font-bold text-xl pt-4 pb-2 mt-2">NTR is a Trash Fetish (PvP Capturing)</h3>
  <i>Enabled for: Invasions (non-Phantasms), Gauntlet, Unregulated</i>
  <p>
  If a member of a retinue is captured by an enemy and held in their territory (whether that territory is purchased from the company or acquired locally) for a period of 24 hours, the hostage loses the benefits of Creature, Stress, Mind, Addiction, Wyldscape, and Corruption Defenses until rescued. The timer begins only when the hostage is both defeated and restrained, and it will reset if they are touched by their master or leave enemy territory unrestrained. The timer will pause if they leave while still restrained by the enemy.  </p>
  <p>Once the timer has run its course, the kidnapper is free to use any bindings, lures, or third-party mind control methods they have, as if the hostage were free. Targets stolen from another contractor are worth 0 capture credits, but may be sold at the usual rate, adjusting for their effective tier after step 3. Recapturing one of your own companions does not grant any credits.</p>

  <h3 class="font-bold text-xl pt-4 pb-2 mt-4">Gauntlet (Continuous PvP)</h3>
  Starting one week into your journey, another contractor will appear in your current world, with their own
  agenda and retinue of waifus and / or husbandos. All opposing contractors will have legal builds. Their budgets
  are equal to your PvP credit value, at chargen or the end of the previous round, plus 1 % of that sum per
  contractor you’ve defeated(non - compounding) and then rounded up to the next whole number.

  <div class="my-4">
    <p>In formula form:</p>
      <code class="text-xl ml-4">P * (1 + E / 100)</code>
    <p>P = Your PvP credit value</p>
    <p>E = Number of eliminated Contractors</p>
  </div>

  <p>Each time you defeat or otherwise handle another contractor, you will have seven days to rest before encountering a
  new one, with more experience and skill than the last. Their expenses will be more optimized; their retinue members
  will be more divergent from their canon selves in ability, equipment, and personality; and the more third - party
  resources they’ll have. The qualitative side of this difficulty ramp stops after 50 victories, though your opponents’
  budgets will continue to increase. The rest period between challengers will decrease by 24 hours for every seven wins
  you accumulate, to a minimum of one hour.</p>
  
  <h3 class="font-bold text-xl pt-4 pb-2">Special Gauntlet Rules</h3>
  <p>Yoink and Substitute are not available in Gauntlet PvP. Unlike in other modes, a new Gauntlet participant’s starting
  world is hidden from them. The client still chooses their starting world’s danger rating, but the master of their
  story chooses which world within that danger rating the client begins in.</p>
  <p>Warranty Plan’s resurrections will not occur until the current match ends. At that point, the winner may pay to
  respawn any slain members of their opponent’s retinue into their own for the purchase price equal to that of the
  member’s effective tier up to step 4. You may also revive slain opponents into your retinue for free, with a one - 
  time genderbend for each if your orientation demands it. These two opportunities are lost if not used. Defeated
  opponents otherwise function identically to your other retinue members. They keep their heritages, but their binding
  methods are replaced by yours.</p>

  <h3 class="font-bold text-xl pt-4 pb-2 mt-4">Gauntlet Teams</h3>
  <p>Co-op play and the Gauntlet are not mutually exclusive. If you do not enter an Arena, the budget values of new opponents will be calculated based on the sum of your team’s PvP credit values, not just yours individually. If you are in an Arena, your opponents will have teams of their own, of equal size to yours. New teams will only enter the fray after a previous team is completely defeated. If the number of remaining opponents in a challenge is less than the size of a team, they will act as individuals according to the non-Arena rule above.</p>
  <p>The number of active participants in an Arena must be a multiple of your team’s size: a team of two may enter any Arena; a team of three may enter Arena II or Arena V; a team of four may enter Arena III or Arena V; a team of five may only enter Arena IV; and a team of six may only enter Arena V.</p>

  <h3 class="font-bold text-xl pt-4 pb-2 mt-2">Life After Defeat</h3>
  <p>You lost. Your opponent stole most or all of your waifus, killed you, and then revived you into their harem so they could rub it in your face for all eternity... but they were defeated in turn and now you’re free. You still have all of your abilities from your Origin, Heritage perks, Template Stacking applied to yourself, and buffs from cross-training (including the results of Talents), equipment (that you still have after your captivity), and any shareable heritage perks that your captor applied to you. We’ll give you another free Stamp, then wipe all of your other previous transactions, resetting your account’s balance to zero. You must repurchase your old control methods, Talents, Defenses, Catch-a-Ride, and other perks at their normal prices.
  </p>
  <p>You may (re-)capture any other freed members of your captor’s former retinue for their regular value (according to their effective tier after step 4). Any former members of your retinue that were stolen from your captor are probably lost, but they may still be out there… somewhere. They will also be treated as normal captures (according to their effective tier after step 4). Any new retinue members that you steal from other contractors besides your captor, whether they’re defeated or not, outside of regulated PvP will be valued at their steal prices (according to their effective tier after step 4). The PvP capture rules above otherwise apply to unregulated PvP. If you had Warranty Plan before your defeat, you may purchase Slightly Used backups of lost retinue members at the price of their final effective tier after step 4.
  </p>
  <p>Good luck.</p>
  `

export const services = `<h3 class="font-bold text-xl pt-4 pb-2">Clientele</h3>
<p>We the company do actually have regular clients. Clients use their own local currency or property to buy clones made from the companions and fаmiliars that Contractors pick up in their operations. These products are stamped with company tattoos keyed to their new owners. A Contractor encountering a Client and their purchase during the course of their operations is rare, but not unheard-of.</p>
`

export const salary = `<h3 class="font-bold text-xl pt-4 pb-2">Contractor Salary</h3>
<p>As one of our contractors, you are entitled to a monthly salary of 10,000 USD or the equivalent in your homeland’s currency. This will be deposited directly into your smart device’s account, so you can convert it to the local money systems of the worlds you visit. On-the-spot fabrication of physical currency or coinage is included at no extra cost.</p>
<p>Bonus pay is also available for each month. This is valued at the sum of the list prices of all captures you’ve made for that month, including earned waifu perks. Imaginary Tier characters are valued according to the exchange rate of their IMG value. For this purpose only, 1 credit is equal to 100 USD.</p>
`

export const helpDesk = `<h3 class="font-bold text-xl pt-4 pb-2">Help Desk Hotline</h3>
<p>If you, a contractor, have questions about any company function, perk, a retinue member’s capabilities, or even is just in a bind locally and doesn’t know what to do next, you need only call the Help Desk and ask. Trained operators are standing by at all times to hear you out. A dedicated app on your smart device allows you to access this free service with just a single tap. Just give a detailed description of your current situation and how it relates to your question, and the operator will answer and (if you’re in a bind) offer some possible solutions. A direct scan of your last seven days of activity will be made - ignoring your <b>Information</b> and <b>Trace Defenses</b> - for our records and to help our operators understand the full context, but don’t worry. Your information is strictly confidential. Not even our operators will remember your situation between the end of one call and the start of the next.</p>
`

export const loans = `
<h3 class="font-bold text-xl pt-4 pb-2">Loans</h3>
<p>You may take loans from the company at any time after chargen. In some situations, you may even find yourself forced to do so. All loans are treated as a single value, your loan balance. This is a separate value from your credit balance.
A starting build on Intensities 0-1 has a loan limit of 500 credits. Intensity 2 reduces that limit to 400 credits; Intensity 3 reduces it to 300; and Intensities 4-5, 200. On intensities 6 and higher, your starting loan limit is 100 credits. For each new world your retinue visits, your loan limit will increase by 100 credits, to a maximum of 5,000.</p>
<p>Interest on your loans is charged every 30 days, from your POV as the contractor, at a fixed rate of 12.5%. You will always have 7 days’ notice before the end of a loan interval. Only the current balance when interest is charged matters, not the peak balance for the month in question. If the interest due at the end of a month is less than half of the total payment you’ve made toward your principal in that month, you will instead not be charged interest for that month.</p>
<p>Interest payments are automatically paid from your credit balance. If you lack sufficient credits, the interest will be added to your loan. If you hit your loan limit or are overcapped, the excess interest will be converted into debt.</p>

<h3 class="font-bold text-xl pt-4 pb-2">Debt</h3>
<p>While your credit balance is below zero, you are in debt. While in debt, you may not make any additional purchases. You may not enter Arranged PvP. Any or all of your Controls and Perks may be disabled, up to and including the basic loyalty effects common to all Bindings. You may be assigned mandatory missions and face additional penalties for failure. All such penalties will only last until your credit balance is no longer below zero.</p>
`

export const missions = `<h3 class="font-bold text-xl pt-4 pb-2">Missions</h3>
<p>Contractors who wish to take a more active role in company operations may accept special missions that serve its interests. These are strictly optional and may be found in an app on the smart device.</p>
<p>Up to nine missions will be available at any time, outside of an active Invasion. A contractor may accept up to three missions at a time. Missions not accepted will not remain indefinitely. If you have at least one empty mission slot, you may reset the list of available missions up to three times per day. Options you turned down when you accepted your third mission will not necessarily still be there when you see the list again.</p>
<p>All missions have a <b>scope</b>, a <b>location</b>, an <b>objective</b>, a <b>reward</b>, and at least one <b>condition</b>. Many missions will have additional objectives nested within them that can variously point toward the overall mission’s next steps or add additional complications with bonus rewards. The mission app’s GUI does not distinguish between a helpful nested objective and a bonus one. Missions can range from somewhat challenging to very, but all missions offered to a contractor can be completed by that contractor and/or their retinue. Some additional purchases may be required or recommended for a given mission, but not anything beyond the contractor’s budget at the time the mission is offered, plus the estimated value of all required nested objectives. Failing a mission means you lose out on the reward, in addition to any casualties your retinue took in the process.</p>
<p>Scope is an estimate of how much time and effort a mission will take, and comes in three broad categories. Quick missions are simple milk runs that can be completed in a single chapter or short story arc (no more than 5,000 words). Standard-length missions are significantly more complex, with a small number of Quick objectives contained within them, and may demand up to 50,000 words to be told properly. Grand missions are the subjects of epic sagas, with a large number of Quick and Standard-length objectives. You may only accept one Grand mission at a time.</p>
<p>A mission’s location may be in any world. If you do not have access to that world, its dimensional coordinates and/or transportation there will be provided for you. Quick missions will always be in a world that you can already access.</p>
<p>Mission objectives vary widely. Sample objectives include:</p>
<ul class="list-disc list-inside ml-2">
  <li>Capture  &lt;target&gt;.</li>
  <li>Capture all canon love interests of &lt;character&gt;. Let &lt;character&gt; know you’ve enslaved them.</li>
  <li>Help &lt;faction&gt; achieve &lt;goal&gt;.</li>
  <li>Defeat &lt;individual/entity/faction&gt; in &lt;conflict type&gt;.</li>
  <li>Your retinue is an antagonist faction. Defeat &lt;setting’s heroes&gt;.</li>
  <li>Find and obtain &lt;object&gt;.</li>
  <li>Defeat &lt;threat&gt;.</li>
  <li>Resettle &lt;population&gt; to a suitable new home.</li>
  <li>Defend &lt;subject&gt; against &lt;threat(s)&gt;.</li>
</ul>

<p>Conditions are added complications that apply to a mission and all nested objectives within it. Nested objectives may have their own conditions as well. Sample conditions include:</p>
<ul class="list-disc list-inside ml-2">
  <li>Leave no trace. No captures, no kills, and no detection allowed.</li>
  <li>No personal involvement. Your retinue must do everything.</li>
  <li>If you want it done right, do it yourself. Your retinue can’t help.</li>
  <li>Forage: Only local captures may help.</li>
  <li>&lt;Company function or perk&gt; is disabled.</li>
  <li>Nerf: All retinue members limited to &lt;tier value&gt; while in &lt;mission setting>.</li>
  <li>Inside Context: Use only powers/technology native to &lt;mission setting>.</li>
  <li>Time Limit, hard: “&lt;Mission&gt; must be completed within &lt;time&gt;.”</li>
  <li>Time Limit, specific: “&lt;Mission&gt; must complete at &lt;time&gt;,”</li>
  <li>Time Limit, soft: “You must complete &lt;mission&gt; before &lt;competition&gt;.”</li>
  <li>Surprise: At least one additional complication may arise part-way through.</li>
</ul>

<p>Mission rewards come in two parts. First, completing a Standard or Grand mission in a world you already have access to will always, immediately, open a new one through the Rainbow Bridge or Exit Stage Left systems.</p>
<p>Second is the choice between a raw credit value and a specific item in the catalog, whether a companion, perk, or what-have-you. The item prize is typically valued around 20-30% higher than the credits, and always in the +15% to +50% range. This second reward is highly variable, dependent on not just the difficulty of the mission (as modified by conditions) but a contractor’s speed and technique. A mission’s scope is highly correlated to its difficulty, while some objectives are naturally harder, and thus more rewarding, than others.</p>
`

export const refund = `<h3 class="font-bold text-xl pt-4 pb-2">Refund and Return Policy</h3>
<p>Origins, Heritages, and your other half in a Couple’s Account (including as part of Danger Rating X starts) are all non-refundable.</p>
<p>All items in the Bindings, Lures and Other Controls, Demiplanes & Dungeons, Talents and Defenses, Catch-a- Ride Perks, Other Perks, and Waifu Perks sections may be refunded at any time, for 80% of the full price you paid. If you return a Control or Perk that gives other items for free, but wish to keep the bundled items, their list prices will be deducted from the standard refund value.</p>
<p>If you return a purchased subject or Ride within two weeks (336 hours) of purchase, you will be refunded the full price you paid, including any waifu perks or Ride add-ons applied to the returnee. Waifu perks applied to the returnee will only be refunded up to 80%.</p>
<p>If you return a purchased subject or Ride after that point, you’ll only receive 80% the price you paid, including the total cost of waifu perks, meta-Talents, and Ride add-ons applied to the returnee.</p>
<p>If you use Evolutionary Engine Array or other means to copy the purchase’s abilities, or otherwise bought that person with the intent of returning them after a task or time, the original purchase will be treated as a rental and capped at 60%.</p>
<p>Like with captures, if a returned subject is damaged or robbed, such that her effective tier at step 3 is less than her tier at step 1a, the return value will instead be relative to the purchase price of her new tier, not her listed tier. This applies to all three of the previous contexts.</p>
<p>IMG refunds are all calculated the same way, but valued in IMG instead of credits. If an IMG refund is not measured in whole tickets, the remainder will be converted into an equivalent number of credits.</p>
`

export const arranged = `<p>Smaller-scale PvP matchmaking is available for contractors who don’t wish to get involved with a career-defining project. This is accessible through a specialized app on your smart device, provided you are not currently participating in PvP and are not in debt..</p>
<p>Contractors choose their desired match settings themselves, that is, the desired number, strength, and experience/ruthlessness/skill of their opponents, and the environment that the match will be fought in. “Random” options are available for each opponent’s skill and strength, as well as the environment. (Note to authors: it’s not actually random on your end.)</p>
<p>Up to twelve contractors may participate in an arranged match, including yourself. No replacements will enter after the match starts.</p>
<p>Opponent skill is sorted into three broad categories: Casual, Average, and Hardcore. Each of these correlates to one of the opponent types in continuous PvP: Negotiable, Rival, and Enemy, respectively.</p>
<p>Strength values are measured on a scale from 1 to 9, as shown in the table to the right. These multipliers are applied to your PvP credit value, with the result rounded to the nearest multiple of 5, to approximate the designated opponent’s budget. Both strength and skill are determined separately for each opponent.</p>
<p>Arranged PvP matches take place in pocket dimensions similar to a contractor’s Demiplane. Any biome for this may be chosen as a preference, whether a cityscape, forest, plain, desert, ocean, mountain range, or even a colonized asteroid or other space setting. The size of an arranged match’s arena and number of usable structures scales to the number of participants, so that every contractor has a starting home turf, all spaced roughly equally far apart from each other. Sufficiently large arenas are likely to have multiple biomes, to accommodate the many contractors’ varying preferences.</p>
<p>Each participant in an arranged match wagers a percent of their PvP credit value upon entering a match. Casual opponents will wager 5%, Average opponents 10%, and Hardcore opponents 15%, all rounded to the nearest multiple of 5. Player contractors are assumed to be Average and thus wager 10%. This is taken out of their accounts when the match starts.</p>
`

export const assetValue = `
<h3 class="font-bold text-xl pt-4 pb-2">PvP Asset Value</h3>
This is the sum of the list prices for you and your retinue’s effective tiers after step 5, plus the combined list prices of your Catch-a-Ride vehicles.
`
export const arrangedConditions = `<h3 class="font-bold text-xl pt-4 pb-2">Arranged Match Wagers, Victory Conditions, and Risk</h3>
<p>Each participant in an arranged match wagers a percent of their PvP credit value upon entering a match. Casual opponents will wager 5%, Average opponents 10%, and Hardcore opponents 15%, all arounded to the nearest multiple of 5. Player contractors are assumed to be Average and thus wager 10%. This is taken out of their accounts when the match starts.</p>
<p>The default victory method in arranged matches is regicide: defeat (capture or kill) all opposing contractors. Their retinues are only a concern as far as they can protect their contractor and will be ejected from the arena. Other styles are also available, including but not limited to:</p>
<ul class="list-disc list-inside">
  <li>Domination: A retinue loses when &lt;% between 66 and 90&gt; of its PvP asset value is defeated.</li>
  <li>King of the Hill: capture and hold &lt;territory&gt;.</li>
  <li>Capture the Flag: capture and hold &lt;people or items&gt;.</li>
  <li>Race: Be the first to complete &lt;mission&gt;.</li>
  <li>Assassin: can only attack &lt;participant&gt; until the target is defeated, then you get a new target. Participants’ targets are hidden from each other. Can still defend against others.</li>
  <li>Mission competitions. Normal credit pay becomes points instead. Winner is the retinue with the most points after the final round.</li>
  <li>Shooting Gallery: Waves of enemy mooks descend on the participants’ positions. Has Limited (winning retinue has the highest score after a set number of rounds) and Survival (winning retinue is the last one standing) variations.</li>
  <li>Presentation: Can be applied to any other ruleset. Points are awarded to each retinue based on style, with a bonus for winning the match according to the normal rules.</li>
</ul>
<p>Defeated participants in an arranged match are merely ejected from the arena, losing only the credits they wagered. All harm or enslavement done to a contractor or their retinue is reversed upon ejection, to up and including death. </p>
`

export const arrangedTeam = `<h3 class="font-bold text-xl pt-4 pb-2">Arranged Team Matches and Prizes</h3>
<p>Team play is compatible with arranged PvP as well. A team only loses when all of its members are defeated. Unlike in continuous PvP, arranged teams do not necessarily have equal strength or skill.</p>
<p>The total of all of a match’s wagers, plus 50% rounded down, is awarded to the winning contractor. A winning team splits the pot evenly, regardless of how many of them remained active to the end. The match’s winner(s) may keep a clone of any one member of their opponents’ retinues that the winner or winner’s retinue defeated during the match - no more than one to each winner. Retinue members that are sold as pairs or trios count as one for this purpose, and only one member of the set needs to be defeated in such cases.</p>
<p>Alternatively, if the winner has the Ancestral Diversity modifier, they may decline all cloned retinue members for a match in favor of the Heritage of an opponent that they or their retinue defeated personally. This grants the winner the root Heritage perk only, bypassing the Ancestral Diversity perk.</p>
`
export const arrangedSpecial = `<h3 class="font-bold text-xl pt-4 pb-2">Special Arranged Match Rules</h3>
<p>Selling an opponent’s retinue member that you captured will merely eject them from the match, with no budgetary effect. All purchases from the company’s store app are blocked during arranged matches. Body, Wild, and Drain Defense are disabled for the duration of the match.</p>
`

export const rip = `<h3 class="font-bold text-xl pt-4 pb-2">R.I.P.</h3>
<p>In the event of your death, your retinue will have three days (72 hours) to revive you before your account is terminated. During this period, your companions or Executor (if you have Only Familiars or a more restrictive intensity option) will gain the ability to interact with contractor-only functions on your smart device. This includes loans. The shopping app will additionally display a list of all potential subjects who could resurrect you, considering all relevant factors.</p>
<p>If you have an automatic resurrection system in place at the time of your death, including an ability that automatically resurrects you from the dead or a retinue member with the ability to resurrect you from the dead, then this mode will not trigger until that automatic resurrection method fails. It is possible to rely on third party resurrection methods to delay the activation of this mode, assuming that you know enough about that third party method to reliably plan around it. If your resurrection method fails, then R.I.P. will trigger at that point. The ability for your waifu to buy a resurrection method on the shopping app does not count as an automatic resurrection method.</p>
<p>If the local world has a “playable” afterlife, that interacts regularly with the mortal and other planes, this ruleset will not trigger until your soul body is destroyed. Additionally, treat your soul body as your regular body for the purposes of Drain and Fatality Defenses.</p>
<p>When your account is terminated, all of your former subjects will be freed and all perks that rely on our backing deleted. This includes our apps on your Smart Device, any Bindings (including physical aspects of Bindings such as individual Tempest pieces), Lures and Other Controls, your demiplane, Talents, Defenses, and Other Perks. Any retinue member or purchased or registered Catch-a-Ride vehicle that is still inside your former demiplane will be ejected into the last world they, personally, visited, as the pocket dimension ceases to exist. Freed retinue members will retain every ability and benefit up to step 3 on the effective tier calculations.</p>
`

export const offspring = `<h3 class="font-bold text-xl pt-4 pb-2">Offspring</h3>
<p>All descendants of you and your subjects are members of your retinue from birth and benefit from all of your perks that apply to all retinue members. This includes artificial creations given life by you or a subject, but not the results of intercourse with beings that use others as hosts for asexual reproduction.</p>
<p>If you have any Binding, they will all have a simpler version of the stamp’s tattoo, a demi-tattoo, ensuring good behavior (according to your and their other parent’s standards) and loyalty to your family, without the other mental effects.</p>
<p>Children of your Extra or Substitute target that you capture are counted as descendants, even if they were already born when you took over. Capture credits are not awarded, however, for any descendant of yours, who was born or even conceived after your entrance, or anyone born into your retinue. The same applies to Sale credits.</p>
<p>If you have <b>No Bindings</b>: Your descendants will not have any bindings, not even the demi-tattoo. You and their mother(s) will have to raise them the hard way.</p>
<p><b>If you’ve created Hybrids</b>: Regardless of whether you used Advanced Runes: Body’s synergy with Absolute Order, Demiplane Laws, or some other method, they will all breed true.</p>
<p>If you have the <b>Symbiote - live births</b>: You may also produce regular humanoid babies - symbiote hybrids with your race (human, kitsune, vulcan, etc.) and the other parent’s - with any member of your swarm. Your descendants will all have the demi-tattoo until puberty, when their symbiote abilities come in.</p>
<p>If you have a <b>Shroud</b>: Your descendants will develop shrouds of their own at puberty, with the specific theme dependent on their personality and interests. They will benefit from all of your perks that apply to your whole retinue, just like their parents, and may form their own retinues centered on themselves. If you have Complementary Colors, any of their ancestors who have a Cloak in your network can keep track of them through a unique Shroud connection. You and any of their ancestors who have a Shroud will not have any direct influence over them beyond the Stamp’s demi-tattoo. Their parents who are not connected by a Shroud or Cloak will not have any direct influence at all.</p>
<p>If you have <b>Lures</b>: Your descendants are immune to your lures. Food from Faerie Feast will still taste wonderful, but the addictive qualities won’t work on them.</p>
<p>If you have <b>Commerce Room: Hotel California</b>: The company suggests abiding by all local laws and barring your descendants from sex work or serving alcohol until they’re of age.</p>
<p>If you have <b>Dragon Scale</b>: Your children will all be dragons as well, with their dragon abilities coming in at puberty and their elements dependent on their personalities and themes. Later generations may hybridize with whatever local races they encounter.</p>
<p>If you are a <b>Transhuman</b>: We at the company never quite figured out how reproduction works for you. Your biological creations, any AIs you construct, or soul hierarchy are sort of like children.</p>
<p class="break-all">If you are an <b>Outsider</b>:██████████████████████████████████████████████████████████████████████████████████████████████████████ ███████████████████████████████████████████████████████████████████████████████</p>
`
export const nasuDLC = `
<h3 class="font-bold text-xl pt-4 pb-2">Pseudo-Servants and Other Alternates</h3>
<p>The following pairs are mutually exclusive at chargen: Saint Kiara and Devil Kiara; Nero and Nero Alter; Medea and Medea Lily; Medusa and Gorgon. If you buy a Pseudo-Servant waifu perk at any time after you purchased or captured them, the Servant’s memories and personality will seamlessly integrate into the host’s, in much the same way as the Memoria perk or Extra and Substitute origins.</p>
<p>Not mutually exclusive: Kama and BB with any Sakura and each other; Ereshkigal and Space Ishtar with any Rin and each other; Scáthach and Skadi; Carmilla and Elizabeth Bathory.</p>
<h3 class="font-bold text-xl pt-4 pb-2">Command Seals</h3>
<p>The following female characters count as valid Masters:
<ul class="list-inside list-disc">
  <li>Fifth Grail War: Tohsaka Rin, Matou Rin, Dark Rin, Matou Sakura, Tohsaka Sakura, Dark Sakura, Beast Rin, Beast Sakura, mainline Illya, Luvia Edelfelt, Bazette, Caren, Kaleido Ruby, Kaleido Sapphire.</li>
  <li>Fourth Grail War: Sola-Ui, Irisviel.</li>
  <li>Moon Cell: Hakuno, Devil Kiara.</li>
  <li>Imperial Grail War: Kohaku.</li>
  <li>Prisma: Prisma Illya, Miyu, Angelica, Berserker Sakura.</li>
  <li>Grand Grail War: Celenike, Fiore, Reika, Jean, Sakura Edelfelt.</li>
  <li>Snowfield Grail War: Prelati, Tiné, Tsubaki.</li>
  <li>Tokyo Grail War: Ayaka, Manaka.</li>
  <li>London: Reines, Gray, Yvette, Hishiri.</li>
  <li>Chaldea: Ritsuka, Yu, Ophelia</li>
  <li>Requiem: Erice.</li>
</ul> 
Pseudo-Servants and Demi-Servants are Servants, not Masters.</p>
<h3 class="font-bold text-xl pt-4 pb-2">Alternate Classes and Outfits</h3>
<p>All Servants with Summer, Santa, or Halloween forms can use the associated skills and NPs at will. The same applies to Atalanta’s, Boudica’s, Da Vinci’s, Nero’s, Nobunaga’s, and Mysterious Heroine X Alter’s alternate classes. Alter Servants and Lily Servants are treated as separate individuals from their standard adult selves, with the exception of Medusa Lancer Alter (to Medusa Rider). The same applies to Elizabeth Báthory and Carmilla, Medusa Rider and Gorgon, and the two adult Artoria Sabers from the two Lancers. When copying a Servant’s power through Template Stacking, all qualifying alternate classes are included in the same template.</p>
<h3 class="font-bold text-xl pt-4 pb-2">Alter Egos</h3>
<p>Purchasing Amaterasu gives you all of the Tamamo Nine as well: Caster Tamamo, Tamamo Cat, and seven others that have not yet appeared in canon. Tamamovich Koyanskya is not a true member. If you have already bought at least one of the Tamamo Nine at the time of purchasing Amaterasu, all credits that you paid for those nine will be refunded. If you instead capture Amaterasu, you will get any applicable refunds for purchased members of the Tamamo Nine and may purchase any missing ones for free. This is not automatic, in case you wish to capture the full set instead. Purchased or captured versions of any Tamamo will remain themselves as they grow more tails, not becoming Amaterasu even if they have a full set of nine.</p>
<p>BB has lost her connection to Nyarlathotep. BB may instead spawn new copies of her Alter Egos if you do not already have them; any that she spawns will not count for capture or sale credits.</p>
<h3 class="font-bold text-xl pt-4 pb-2">Servants</h3>
<p>Captured Servants are no longer connected to the Throne of Heroes and will not affect their greater Heroic Spirits. All Servants exist in a quantum state where they are both fully incarnated and able to enter Spirit Form. Servants benefit from their homeland bonus in any version of their homeland, whether the past, their own time, the present, or the future. This does not include unrelated counterpart cultures and replaces the general background magical field rule. The body-modification suite is capable of changing Servants who have Golden Rule (Body), Divine Core of the Goddess, and other such skills.</p>
<p><b>Saber</b>: Empyreal Eye overuse may cause Musashi to travel between worlds and eras on her own; if that happens and you have Rainbow Bridge, the portal room will connect to her new location as if it were intentional. She can learn to control this ability with practice. Artoria Saber (not Alter) has the souped-up Yamaha V-Max originally given to her in the Fourth Grail War. Artoria Saber (Alter) has Cavall the 2nd as a fаmiliar. Nero Claudius may access her Saber Venus form if you also have Altera. Okita’s summer cyborgization cured her tuberculosis in canon; it remains cured here even if she only uses her Saber abilities.</p>
<p><b>Archer</b>: The true love condition for Tomoe Gozen’s NP is transferred to you. Artemis has the mascot version of Orion as a fаmiliar. Ishtar has Gugalanna Lily as a fаmiliar.</p>
<p><b>Lancer</b>: Artoria Lancer (not Alter) benefits from Goddess Rhongomyniad’s added power; the two will be treated as the same person. Purchased versions of Gareth have the memories of her fairy self from Lostbelt 6, in addition to her PHH version.</p>
<p><b>Rider</b>: Sakamoto Ryouma has Oryou as a humanoid fаmiliar; if you already have Oryou when you buy Ryouma, you’ll be refunded her full cost. Saint Martha has her dragon Tarrasque as a fаmiliar. Nero Alter has Beast VI, the Beast of the Sea, as a fаmiliar. This is a different Beast VI than the one Manaka can summon. Habetrot will not be harmed by touching advanced technology, including her Black Barrel; purchased versions of her will have her memories from both Pan-Human History and Lostbelt 6.</p>
<p><b>Assassin</b>: Hundred Faces Hassan’s main body is the adult female one. Carmilla’s Femme Fatale (False) does not affect her feelings toward you. Shuten Douji and Ibuki Douji are separate beings. Ibuki may be brought forth if you involve yourself in the events of Lostbelt 5.5, but the two oni are otherwise purchased and captured separately. Tamamovich Koyanskaya’s Independent Manifestation is limited to one local multiverse at a time.</p>
<p><b>Avenger</b>: Space Ishtar (Ishtar Astoreth) can split off Astoreth and Space Ishtar/Rin into their own bodies, at will, and re-combine with them on contact.</p>
<p><b>Ruler</b>: The Morgan le Fay of Pan-Human History has three distinct selves: her human aspect, Morgan, and her fairy aspect, Viviane, alongside her aspect as the embodiment of Britain and inheritor of its Mystery, Morgan le Fay. She can split off Morgan and Viviane into their own bodies, at will, and re-combine with them on contact.</p>
<p><b>Alter Ego</b>: Kingprotea can control her size, all the way down to regular human height or the smallest you’ve been since the start of your journey, whichever is smaller.</p>
<p><b>Foreigner</b>: Katsushika Oui has Tokitarou as a fаmiliar; he’ll remain in the mental backseat while the two are fused.</p>
`
